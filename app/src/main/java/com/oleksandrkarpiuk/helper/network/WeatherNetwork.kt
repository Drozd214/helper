package com.oleksandrkarpiuk.helper.network

abstract class WeatherNetwork {

    companion object {

        const val BASE_URL = "http://api.weatherapi.com"
    }
}